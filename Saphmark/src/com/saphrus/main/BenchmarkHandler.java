package com.saphrus.main;

import com.saphrus.algorithms.HashGenerationException;
import com.saphrus.algorithms.HashUtils;

import java.lang.Math;
import java.util.ArrayList;
import java.util.List;

public class BenchmarkHandler {
	// Tracks all the tests to run
	public List < String > testsToRun = new ArrayList < String > ();

	// Tracks all the scores
	public List < Long > scores = new ArrayList < Long > ();
	public List < String > scoreNames = new ArrayList < String > ();
	
	// Benchmark
	public boolean stopRequested = false;

	void startBenchmark() {
		// Is it benchmarking
		boolean benchmarking = true;
		// Check if tests are picked
		if (testsToRun.size() == 0) {
			benchmarking = false;
		}
		// Track current test number
		int testNumber = 0;
		while (benchmarking) {
			// Check if stop has been requested
			if (stopRequested) {
				benchmarking = false;
			}
			String testToRun = testsToRun.get(testNumber);
			switch (testToRun) {
				case "cpuAlgorithmTest1":
					cpuAlgorithmTest1();
				case "cpuAlgorithmTest2":
					cpuAlgorithmTest2();
				case "cpuAlgorithmTest3":
					cpuAlgorithmTest3();
				case "cpuAlgorithmTest4":
					cpuAlgorithmTest4();
				case "cpuAlgorithmTest5":
					cpuAlgorithmTest5();
				case "cpuAlgorithmTest6":
					cpuAlgorithmTest6();
				case "cpuAlgorithmTest7":
					cpuAlgorithmTest7();
				case "cpuAlgorithmTest8":
					cpuAlgorithmTest8();
				case "GPUTest1":
					GPUTest1();
				case "GPUTest2":
					GPUTest2();
			}
			scoreNames.add(testsToRun.get(testNumber));
			// Sleep thread for cool down
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			testNumber++;
			if (testNumber > testsToRun.size() - 1) {
				System.out.println("Benchmark Finished");
				benchmarking = false;
			}
		}
	}


	/* Single Core CPU Tests
	 * - 4 Hashing Tests (Using MD5, SHA-1, SHA-256 & SHA-512)
	 * - 2 Math Tests (Using Sin, Cos & Tan)
	 * - 2 Incrementing Tests
	 */

	void cpuAlgorithmTest1() {
		long startTime = System.nanoTime();
		for (int i = 0; i < 20000000; i++) {
			try {
				HashUtils.generateMD5(Integer.toString(i));
			} catch (HashGenerationException e) {
				e.printStackTrace();
			}
		}
		long timeTaken = System.nanoTime() - startTime;
		scores.add(timeTaken / 1000000);
	}

	void cpuAlgorithmTest2() {
		long startTime = System.nanoTime();
		for (int i = 0; i < 20000000; i++) {
			try {
				HashUtils.generateSHA1(Integer.toString(i));
			} catch (HashGenerationException e) {
				e.printStackTrace();
			}
		}
		long timeTaken = System.nanoTime() - startTime;
		scores.add(timeTaken / 1000000);
	}

	void cpuAlgorithmTest3() {
		long startTime = System.nanoTime();
		for (int i = 0; i < 20000000; i++) {
			try {
				HashUtils.generateSHA256(Integer.toString(i));
			} catch (HashGenerationException e) {
				e.printStackTrace();
			}
		}
		long timeTaken = System.nanoTime() - startTime;
		scores.add(timeTaken / 1000000);
	}

	void cpuAlgorithmTest4() {
		long startTime = System.nanoTime();
		for (int i = 0; i < 20000000; i++) {
			try {
				HashUtils.generateSHA512(Integer.toString(i));
			} catch (HashGenerationException e) {
				e.printStackTrace();
			}
		}
		long timeTaken = System.nanoTime() - startTime;
		scores.add(timeTaken / 1000000);
	}

	void cpuAlgorithmTest5() {
		long startTime = System.nanoTime();
		float number = 0;
		while (number < 10000000) {
			number += Math.abs(Math.sin(1));
		}
		number = 0;
		while (number < 10000000) {
			number += Math.abs(Math.cos(1));
		}
		number = 0;
		while (number < 10000000) {
			number += Math.abs(Math.tan(1));
		}
		long timeTaken = System.nanoTime() - startTime;
		scores.add(timeTaken / 1000000);
	}

	void cpuAlgorithmTest6() {
		long startTime = System.nanoTime();
		float number = 0;
		while (number < 10000000) {
			number += 1f / Math.abs(Math.sin(1));
		}
		number = 0;
		while (number < 10000000) {
			number += 1f / Math.abs(Math.cos(1));
		}
		number = 0;
		while (number < 10000000) {
			number += 1f / Math.abs(Math.tan(1));
		}
		long timeTaken = System.nanoTime() - startTime;
		scores.add(timeTaken / 1000000);
	}
	
	void cpuAlgorithmTest7() {
		long startTime = System.nanoTime();
		float number = 0;
		while (number < 1000000) {
			number += 0.5f;
		}
		long timeTaken = System.nanoTime() - startTime;
		scores.add(timeTaken / 1000000);
	}
	
	void cpuAlgorithmTest8() {
		long startTime = System.nanoTime();
		float number = 0;
		while (number > -1000000) {
			number -= 0.5f;
		}
		long timeTaken = System.nanoTime() - startTime;
		scores.add(timeTaken / 1000000);
	}

	/* GPU Tests
	 * - Triangle Generation
	 * - Square Generation
	 * 
	 */
	void GPUTest1 () {
		DisplayHandler displayHandler = new DisplayHandler();
		long timeTaken = displayHandler.GPUTest1();
		scores.add(timeTaken / 1000000);
	}
	
	void GPUTest2 () {
		DisplayHandler displayHandler = new DisplayHandler();
		long timeTaken = displayHandler.GPUTest2();
		scores.add(timeTaken / 1000000);
	}
}