package com.saphrus.main;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

import com.saphrus.rendering.DrawUtils;

public class DisplayHandler {
	
	// Variables
	int resolutionWidth = 1024;
	int resolutionHeight = 768;
	int a = 0;
	boolean shouldClose = false;
	
	long GPUTest1() {
		try {
			// Set the resolution to 1080p
			Display.setDisplayMode(new DisplayMode(resolutionWidth, resolutionHeight));
			// Set the title
			Display.setTitle("Saphmark");
			// Set VSync off
			Display.setVSyncEnabled(false);
			// Turn full screen on
			Display.setFullscreen(true);
			// Create the display
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		// Initialize OpenGL
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		//GL11.glFrustum(0, 1024, 0, 768, 1, -1);
		GL11.glOrtho(0, 1024, 0, 768, 1, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		
		long startTime = System.nanoTime();
		for (int i = 0; i < 100000; i++) {
		    // Clear the screen and depth buffer
		    GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);	
		    
			// Draw Shapes
			DrawUtils.generateRandomTriangle1();
			DrawUtils.generateRandomTriangle2();
			DrawUtils.generateRandomTriangle3();
			DrawUtils.generateRandomTriangle4();

			// Render the display
			Display.update();
			
			// Set the update rate
			Display.sync(3600000);
		}
		Display.destroy();
		return System.nanoTime() - startTime;
	}
	
	long GPUTest2() {
		try {
			// Set the resolution to 1080p
			Display.setDisplayMode(new DisplayMode(resolutionWidth, resolutionHeight));
			// Set the title
			Display.setTitle("Saphmark");
			// Set VSync off
			Display.setVSyncEnabled(false);
			// Turn full screen on
			Display.setFullscreen(true);
			// Create the display
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		// Initialize OpenGL
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		//GL11.glFrustum(0, 1024, 0, 768, 1, -1);
		GL11.glOrtho(0, 1024, 0, 768, 1, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		
		long startTime = System.nanoTime();
		for (int i = 0; i < 100000; i++) {
		    // Clear the screen and depth buffer
		    GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);	
		    
			// Draw Shapes
			DrawUtils.generateRandomSquare1();
			DrawUtils.generateRandomSquare1();
			DrawUtils.generateRandomSquare1();
			DrawUtils.generateRandomSquare1();
			DrawUtils.generateRandomSquare1();
			DrawUtils.generateRandomSquare1();
			DrawUtils.generateRandomSquare1();
			DrawUtils.generateRandomSquare1();
			DrawUtils.generateRandomSquare1();
			DrawUtils.generateRandomSquare1();
			DrawUtils.generateRandomSquare1();
			DrawUtils.generateRandomSquare1();

			// Render the display
			Display.update();
			
			// Set the update rate
			Display.sync(3600000);
		}
		Display.destroy();
		return System.nanoTime() - startTime;
	}
}
