package com.saphrus.main;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;
import javax.swing.JSeparator;

import java.awt.EventQueue;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import java.awt.Font;
import java.awt.Component;

import javax.swing.JButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBox;

public class UIHandler {
	
	BenchmarkHandler benchmarkHandler = new BenchmarkHandler();
	
	JFrame frame;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIHandler window = new UIHandler();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public UIHandler() {
		handleUI();
	}
	
	/**
	 * @wbp.parser.entryPoint
	 */
	void handleUI() {
		// Enable the Nimbus Look and Feel
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
			// Fallback Look and Feel is Metal
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Metal".equals(info.getName())) {
		            try {
						UIManager.setLookAndFeel(info.getClassName());
					} catch (Exception e1) {
						e1.printStackTrace();
					}
		            break;
		        }
		    }
		}
		
		// Setup the frame
		frame = new JFrame("Saphmark");
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Saphmark");
	    frame.setSize(1280, 720);
		
	    ImageIcon imgSaphmark = new ImageIcon("images/Saphmark_Logo.png");
		JLabel lblSaphmark = new JLabel(imgSaphmark);
		lblSaphmark.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblSaphmark.setBounds(371, 30, 559, 130);
		frame.getContentPane().add(lblSaphmark);
		
		JLabel lblDetailedScores = new JLabel("Detailed Scores");
		lblDetailedScores.setFont(new Font("Rockwell", Font.PLAIN, 24));
		lblDetailedScores.setHorizontalTextPosition(SwingConstants.CENTER);
		lblDetailedScores.setHorizontalAlignment(SwingConstants.CENTER);
		lblDetailedScores.setAlignmentX(0.5f);
		lblDetailedScores.setBounds(0, 6, 200, 663);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(-4, -4, 250, 672);
		scrollPane.setViewportBorder(null);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setViewportView(lblDetailedScores);
		frame.getContentPane().add(scrollPane);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(230, 120, 1107, 50);
		frame.getContentPane().add(separator);
		
		JButton btnStartBenchmark = new JButton("Start Benchmark");
		btnStartBenchmark.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				benchmarkHandler.stopRequested = false;
				benchmarkHandler.startBenchmark();
			}
		});
		btnStartBenchmark.setFont(new Font("SansSerif", Font.PLAIN, 16));
		btnStartBenchmark.setBounds(370, 170, 200, 50);
		frame.getContentPane().add(btnStartBenchmark);
		
		JButton btnStopBenchmark = new JButton("Stop Benchmark");
		btnStopBenchmark.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				benchmarkHandler.stopRequested = true;
			}
		});
		btnStopBenchmark.setFont(new Font("SansSerif", Font.PLAIN, 16));
		btnStopBenchmark.setBounds(960, 170, 200, 50);
		frame.getContentPane().add(btnStopBenchmark);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(230, 280, 1069, 23);
		frame.getContentPane().add(separator_1);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setOrientation(SwingConstants.VERTICAL);
		separator_2.setBounds(450, 280, 2, 388);
		frame.getContentPane().add(separator_2);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setOrientation(SwingConstants.VERTICAL);
		separator_3.setBounds(650, 280, 2, 388);
		frame.getContentPane().add(separator_3);
		
		JLabel lblCpu = new JLabel("CPU");
		lblCpu.setFont(new Font("Tahoma", Font.PLAIN, 36));
		lblCpu.setBounds(256, 280, 66, 58);
		frame.getContentPane().add(lblCpu);
		
		JLabel lblHashingTests = new JLabel("Hashing Tests");
		lblHashingTests.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblHashingTests.setBounds(256, 330, 150, 25);
		frame.getContentPane().add(lblHashingTests);
		
		JCheckBox chckbxMD5 = new JCheckBox("MD5");
		chckbxMD5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String test = "cpuAlgorithmTest1";
				if (chckbxMD5.isSelected()) {
					for (int i = 0; i < benchmarkHandler.testsToRun.size(); i++) {
						if (test == benchmarkHandler.testsToRun.get(i)) {
							benchmarkHandler.testsToRun.remove(i);
						}
					}
					benchmarkHandler.testsToRun.add(test);
				} else {
					benchmarkHandler.testsToRun.add(test);
				}
			}
		});
		chckbxMD5.setBackground(Color.WHITE);
		chckbxMD5.setBounds(252, 362, 97, 23);
		frame.getContentPane().add(chckbxMD5);
		
		JCheckBox chckbxSha_1 = new JCheckBox("SHA-1");
		chckbxSha_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String test = "cpuAlgorithmTest2";
				if (chckbxSha_1.isSelected()) {
					for (int i = 0; i < benchmarkHandler.testsToRun.size(); i++) {
						if (test == benchmarkHandler.testsToRun.get(i)) {
							benchmarkHandler.testsToRun.remove(i);
						}
					}
					benchmarkHandler.testsToRun.add(test);
				} else {
					benchmarkHandler.testsToRun.add(test);
				}
			}
		});
		chckbxSha_1.setBackground(Color.WHITE);
		chckbxSha_1.setBounds(252, 388, 97, 23);
		frame.getContentPane().add(chckbxSha_1);
		
		JCheckBox chckbxSha_2 = new JCheckBox("SHA-256");
		chckbxSha_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String test = "cpuAlgorithmTest3";
				if (chckbxSha_2.isSelected()) {
					for (int i = 0; i < benchmarkHandler.testsToRun.size(); i++) {
						if (test == benchmarkHandler.testsToRun.get(i)) {
							benchmarkHandler.testsToRun.remove(i);
						}
					}
					benchmarkHandler.testsToRun.add(test);
				} else {
					benchmarkHandler.testsToRun.add(test);
				}
			}
		});
		chckbxSha_2.setBackground(Color.WHITE);
		chckbxSha_2.setBounds(252, 414, 97, 23);
		frame.getContentPane().add(chckbxSha_2);
		
		JCheckBox chckbxSha_3 = new JCheckBox("SHA-512");
		chckbxSha_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String test = "cpuAlgorithmTest4";
				if (chckbxSha_3.isSelected()) {
					for (int i = 0; i < benchmarkHandler.testsToRun.size(); i++) {
						if (test == benchmarkHandler.testsToRun.get(i)) {
							benchmarkHandler.testsToRun.remove(i);
						}
					}
					benchmarkHandler.testsToRun.add(test);
				} else {
					benchmarkHandler.testsToRun.add(test);
				}
			}
		});
		chckbxSha_3.setBackground(Color.WHITE);
		chckbxSha_3.setBounds(252, 440, 97, 23);
		frame.getContentPane().add(chckbxSha_3);
		
		JLabel lblMathTest = new JLabel("Math Tests");
		lblMathTest.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMathTest.setBounds(256, 470, 150, 25);
		frame.getContentPane().add(lblMathTest);
		
		JCheckBox chckbxDivison = new JCheckBox("Sin Cos Tan Addition");
		chckbxDivison.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String test = "cpuAlgorithmTest5";
				if (chckbxDivison.isSelected()) {
					for (int i = 0; i < benchmarkHandler.testsToRun.size(); i++) {
						if (test == benchmarkHandler.testsToRun.get(i)) {
							benchmarkHandler.testsToRun.remove(i);
						}
					}
					benchmarkHandler.testsToRun.add(test);
				} else {
					benchmarkHandler.testsToRun.add(test);
				}
			}
		});
		chckbxDivison.setBackground(Color.WHITE);
		chckbxDivison.setBounds(252, 502, 144, 23);
		frame.getContentPane().add(chckbxDivison);
		
		JCheckBox chckbxSinCosTan = new JCheckBox("Sin Cos Tan Division");
		chckbxSinCosTan.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String test = "cpuAlgorithmTest6";
				if (chckbxSinCosTan.isSelected()) {
					for (int i = 0; i < benchmarkHandler.testsToRun.size(); i++) {
						if (test == benchmarkHandler.testsToRun.get(i)) {
							benchmarkHandler.testsToRun.remove(i);
						}
					}
					benchmarkHandler.testsToRun.add(test);
				} else {
					benchmarkHandler.testsToRun.add(test);
				}
			}
		});
		chckbxSinCosTan.setBackground(Color.WHITE);
		chckbxSinCosTan.setBounds(252, 525, 144, 23);
		frame.getContentPane().add(chckbxSinCosTan);
		
		JLabel lblIncrementationTests = new JLabel("Incrementation Tests");
		lblIncrementationTests.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblIncrementationTests.setBounds(256, 555, 160, 25);
		frame.getContentPane().add(lblIncrementationTests);
		
		JCheckBox chckbxAddition = new JCheckBox("Addition");
		chckbxAddition.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String test = "cpuAlgorithmTest7";
				if (chckbxAddition.isSelected()) {
					for (int i = 0; i < benchmarkHandler.testsToRun.size(); i++) {
						if (test == benchmarkHandler.testsToRun.get(i)) {
							benchmarkHandler.testsToRun.remove(i);
						}
					}
					benchmarkHandler.testsToRun.add(test);
				} else {
					benchmarkHandler.testsToRun.add(test);
				}
			}
		});
		chckbxAddition.setBackground(Color.WHITE);
		chckbxAddition.setBounds(252, 587, 144, 23);
		frame.getContentPane().add(chckbxAddition);
		
		JCheckBox chckbxSubtraction = new JCheckBox("Subtraction");
		chckbxSubtraction.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String test = "cpuAlgorithmTest8";
				if (chckbxSubtraction.isSelected()) {
					for (int i = 0; i < benchmarkHandler.testsToRun.size(); i++) {
						if (test == benchmarkHandler.testsToRun.get(i)) {
							benchmarkHandler.testsToRun.remove(i);
						}
					}
					benchmarkHandler.testsToRun.add(test);
				} else {
					benchmarkHandler.testsToRun.add(test);
				}
			}
		});
		chckbxSubtraction.setBackground(Color.WHITE);
		chckbxSubtraction.setBounds(252, 613, 144, 23);
		frame.getContentPane().add(chckbxSubtraction);
		
		JLabel lblGpu = new JLabel("GPU");
		lblGpu.setFont(new Font("Tahoma", Font.PLAIN, 36));
		lblGpu.setBounds(462, 280, 80, 58);
		frame.getContentPane().add(lblGpu);
		
		JLabel lblDrawingTests = new JLabel("Drawing Tests");
		lblDrawingTests.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDrawingTests.setBounds(462, 330, 150, 25);
		frame.getContentPane().add(lblDrawingTests);
		
		JCheckBox chckbxRandomTriangles = new JCheckBox("Random Triangles");
		chckbxRandomTriangles.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String test = "GPUTest1";
				if (chckbxRandomTriangles.isSelected()) {
					for (int i = 0; i < benchmarkHandler.testsToRun.size(); i++) {
						if (test == benchmarkHandler.testsToRun.get(i)) {
							benchmarkHandler.testsToRun.remove(i);
						}
					}
					benchmarkHandler.testsToRun.add(test);
				} else {
					benchmarkHandler.testsToRun.add(test);
				}
			}
		});
		chckbxRandomTriangles.setBackground(Color.WHITE);
		chckbxRandomTriangles.setBounds(458, 362, 186, 23);
		frame.getContentPane().add(chckbxRandomTriangles);
		
		JCheckBox chckbxRandomSquares = new JCheckBox("Random Squares");
		chckbxRandomSquares.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String test = "GPUTest2";
				if (chckbxRandomSquares.isSelected()) {
					for (int i = 0; i < benchmarkHandler.testsToRun.size(); i++) {
						if (test == benchmarkHandler.testsToRun.get(i)) {
							benchmarkHandler.testsToRun.remove(i);
						}
					}
					benchmarkHandler.testsToRun.add(test);
				} else {
					benchmarkHandler.testsToRun.add(test);
				}
			}
		});
		chckbxRandomSquares.setBackground(Color.WHITE);
		chckbxRandomSquares.setBounds(458, 388, 186, 23);
		frame.getContentPane().add(chckbxRandomSquares);
	    
	    JMenuBar menuBar = new JMenuBar();
	    frame.setJMenuBar(menuBar);
	    
	    JMenu mnFile = new JMenu("File");
	    mnFile.setFont(new Font("SansSerif", Font.PLAIN, 18));
	    menuBar.add(mnFile);
	    
	    JMenuItem mntmStartBenchmark = new JMenuItem("Start Benchmark");
	    mntmStartBenchmark.setFont(new Font("SansSerif", Font.PLAIN, 16));
	    mnFile.add(mntmStartBenchmark);
	    
	    JMenuItem mntmSaveResult = new JMenuItem("Save Result");
	    mntmSaveResult.setFont(new Font("SansSerif", Font.PLAIN, 16));
	    mnFile.add(mntmSaveResult);
	    
	    JMenu mnView = new JMenu("View");
	    mnView.setFont(new Font("SansSerif", Font.PLAIN, 18));
	    menuBar.add(mnView);
	    
	    JMenuItem mntmBenchmarkProgressBar = new JMenuItem("Progress Bar");
	    mntmBenchmarkProgressBar.setFont(new Font("SansSerif", Font.PLAIN, 16));
	    mnView.add(mntmBenchmarkProgressBar);
	    
	    JMenuItem mntmSystemSpecifications = new JMenuItem("System Specifications");
	    mntmSystemSpecifications.setFont(new Font("SansSerif", Font.PLAIN, 16));
	    mnView.add(mntmSystemSpecifications);
	}
}
