package com.saphrus.rendering;

import org.lwjgl.opengl.GL11;
import java.util.Random;

public class DrawUtils {
	
	/*
	 * Random 2D Shapes
	 */
	public static void generateRandomTriangle1()
	{
		// Positioning
		Random random = new Random();
		int yPosition = random.nextInt(1920) - 300;
		int xPosition = random.nextInt(1080) - 300;
		int scale = random.nextInt(10);
		
		// Set the color of the square (R,G,B,A)
	    GL11.glColor4f(random.nextFloat(),random.nextFloat(),random.nextFloat(),random.nextFloat());
 
	    // Draw the Square
	    GL11.glBegin(GL11.GL_TRIANGLES);
		GL11.glVertex2f(xPosition+64*scale,yPosition+64*scale);
		GL11.glVertex2f(xPosition,yPosition);
		GL11.glVertex2f(xPosition+64*scale,yPosition);
	    GL11.glEnd();
	}
	
	public static void generateRandomTriangle2()
	{
		// Positioning
		Random random = new Random();
		int yPosition = random.nextInt(1920) - 300;
		int xPosition = random.nextInt(1080) - 300;
		int scale = random.nextInt(5);
		
		// Set the color of the square (R,G,B,A)
	    GL11.glColor4f(random.nextFloat(),random.nextFloat(),random.nextFloat(),random.nextFloat());
 
	    // Draw the Square
	    GL11.glBegin(GL11.GL_TRIANGLES);
		GL11.glVertex2f(xPosition,yPosition+64*scale);
		GL11.glVertex2f(xPosition+64*scale,yPosition+64*scale);
		GL11.glVertex2f(xPosition+64*scale,yPosition);
	    GL11.glEnd();
	}
	
	public static void generateRandomTriangle3()
	{
		// Positioning
		Random random = new Random();
		int yPosition = random.nextInt(1920) - 300;
		int xPosition = random.nextInt(1080) - 300;
		int scale = random.nextInt(5);
		
		// Set the color of the square (R,G,B,A)
	    GL11.glColor4f(random.nextFloat(),random.nextFloat(),random.nextFloat(),random.nextFloat());
 
	    // Draw the Square
	    GL11.glBegin(GL11.GL_TRIANGLES);
		GL11.glVertex2f(xPosition,yPosition+64*scale);
		GL11.glVertex2f(xPosition+64*scale,yPosition);
		GL11.glVertex2f(xPosition,yPosition);
	    GL11.glEnd();
	}
	
	public static void generateRandomTriangle4()
	{
		// Positioning
		Random random = new Random();
		int yPosition = random.nextInt(1920) - 300;
		int xPosition = random.nextInt(1080) - 300;
		int scale = random.nextInt(5);
		
		// Set the color of the square (R,G,B,A)
	    GL11.glColor4f(random.nextFloat(),random.nextFloat(),random.nextFloat(),random.nextFloat());
 
	    // Draw the Square
	    GL11.glBegin(GL11.GL_TRIANGLES);
		GL11.glVertex2f(xPosition,yPosition+64*scale);
		GL11.glVertex2f(xPosition+64*scale,yPosition);
		GL11.glVertex2f(xPosition+64*scale,yPosition);
	    GL11.glEnd();
	}
	
	public static void generateRandomSquare1()
	{
		// Positioning
		Random random = new Random();
		int yPosition = random.nextInt(1920) - 300;
		int xPosition = random.nextInt(1080) - 300;
		int scale = random.nextInt(5);
		
		// Set the color of the square (R,G,B,A)
	    GL11.glColor4f(random.nextFloat(),random.nextFloat(),random.nextFloat(),random.nextFloat());
 
	    // Draw the Square
	    GL11.glBegin(GL11.GL_QUADS);
	    GL11.glVertex2f(xPosition,yPosition);
		GL11.glVertex2f(xPosition+64*scale,yPosition);
		GL11.glVertex2f(xPosition+64*scale,yPosition+64*scale);
		GL11.glVertex2f(xPosition,yPosition+64*scale);
	    GL11.glEnd();
	}
}
